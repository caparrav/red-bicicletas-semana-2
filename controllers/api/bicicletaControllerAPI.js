var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req,res){
    Bicicleta.allBicis(function(err,bicis) {
        res.status(200).json({
            bicicletas: bicis,
        });
    });
}

exports.bicicleta_create = function(req,res){
    var bici = new Bicicleta({code: req.body.id, color: req.body.color,modelo: req.body.modelo})
    bici.ubicacion = [req.body.lat,req.body.lng];
    Bicicleta.add(bici)
    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req,res){
    Bicicleta.removeByCode(req.body.id,function(err){
        res.status(204).send();
    }); 
};


exports.bicicleta_update = function(req,res){
    const update = {code: req.body.id, color: req.body.color, modelo:req.body.modelo, ubicacion:[req.body.lat,req.body.lng]};
    Bicicleta.updateByCode(req.body.id,update,function(err) {
        Bicicleta.findByCode(req.body.id,function(err,bici) {
            res.status(200).json(bici);
        });
    });
}


