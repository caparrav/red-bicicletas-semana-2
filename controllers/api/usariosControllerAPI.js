var Usuario = require('../../models/usuario');

exports.usuarios = function(req,res){
    Usuario.find({},function(err,usuarios){
        res.status(200).json({
            usuarios: usuarios,
        });
    });
};

exports.usuarios_create = function(req,res){
    var usuario = new Usuario({nombre: req.body.nombre});
    usuario.save(function(err){
        res.status(200).json(usuario);
    });    
};

exports.usuarios_reservar = function(req,res){
    Usuario.findById(req.body.id,function(err,usuario){
        console.log(usuario);
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err){
            console.log('reserva!!!!');
            res.status(200).send();
        });
    });
};



// exports.usuarios_delete = function(req,res){
//     Bicicleta.removeById(req.body.id)
//     res.status(204).send();
// }

// exports.usuarios_update = function(req,res){
//     var bici = Bicicleta.findById(req.body.id);
//     bici.color= req.body.color;
//     bici.modelo= req.body.modelo;
//     bici.ubicacion = [req.body.lat,req.body.lng];
//     res.status(200).json({
//         bicicleta: bici,
//     });
// }