var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");

describe('Testing Bicicletas', function(){
    
    beforeAll(async ()=>{
        await mongoose.disconnect();
    });

    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser: true })
        const db = mongoose.connection;
        db.on('error', console.error.bind(console,'connection error'));
        db.once('open',function(){
            console.log('we are connected to test database');
            done();
        });    
    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function( err, success){
            if (err) console.log(err);
            mongoose.disconnect(err); 
            done();
        });
    });

    describe('Bicicleta.createInstance',()=>{
        it('crea una instancia bicicleta',()=>{
            var bici =Bicicleta.createInstance(1,"verde","urbana",[-34.5,54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde")
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(54.1);
        });
    });

    describe('Bicicleta.allBicis',()=>{
        it('comienza vacia', function(done){
            Bicicleta.allBicis(function(err,bicis) {
                    expect(bicis.length).toBe(0);
                    done();
            });
        });
    });


    describe('Bicicleta.add',() =>{
        it('agrega solo una bici', (done)=>{
            var aBici = new Bicicleta({code: 1,color: "verde",modelo: "urbana",ubicacion: [-34.5,54.1]});
            Bicicleta.add(aBici,function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode',()=>{
        it('debe devolver la bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                var aBici1 = new Bicicleta({
                        code: 1,
                        color: "verde",
                        modelo: "urbana",
                        ubicacion: [-34.5,54.1]
                    });
                Bicicleta.add(aBici1,function(err,newBici){
                    if (err) console.log(err);
                    
                    var aBici2 = new Bicicleta({
                        code: 2,
                        color: "roja",
                        modelo: "urbana",
                        ubicacion: [-34.5,54.1]});
                    Bicicleta.add(aBici2,function(err,newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (error, targetBici){
                            expect(targetBici.code).toBe(aBici1.code);
                            expect(targetBici.color).toBe(aBici1.color);
                            expect(targetBici.modelo).toBe(aBici1.modelo);

                            done();
                        });
                    });
                }); 
            });   
        });
    });


    describe('Bicicleta.deleteByCode',()=>{
        it('debe eliminar la bici con code 1', (done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                var aBici1 = new Bicicleta({
                        code: 1,
                        color: "verde",
                        modelo: "urbana",
                        ubicacion: [-34.5,54.1]
                    });
                Bicicleta.add(aBici1,function(err,newBici){
                    if (err) console.log(err);
                    
                    var aBici2 = new Bicicleta({
                        code: 2,
                        color: "roja",
                        modelo: "urbana",
                        ubicacion: [-34.5,54.1]});
                    Bicicleta.add(aBici2,function(err,newBici){
                        if (err) console.log(err);
                        //console.log(newBici)
                        Bicicleta.removeByCode(1, function(err,rBici){
                            if (err) console.log(err);
                            Bicicleta.allBicis(function(err,bicis){
                                if (err) console.log(err);
                                expect(bicis.length).toBe(1);
                                done();
                            });
                        });
                    });
                }); 
            });   
        });
    });

    describe('Bicicleta.updateBycode',()=>{
        it('debe actualizar una bici', (done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                var aBici1 = new Bicicleta({
                        code: 1,
                        color: "verde",
                        modelo: "ruta",
                        ubicacion: [-34.5,54.1]
                    });
                Bicicleta.add(aBici1,function(err,newBici){
                    if (err) console.log(err);
                    var aBici2 = new Bicicleta({
                        code: 2,
                        color: "roja",
                        modelo: "urbana",
                        ubicacion: [-34.5,54.1]});
                    Bicicleta.add(aBici2,function(err,newBici){
                        if (err) console.log(err);
                        var update = {color: "verde", modelo: "ruta", ubicacion: [-34.32423,54.3123123]};            
                        Bicicleta.updateByCode(2,update,function(err){
                                if (err) console.log(err);
                                Bicicleta.findByCode(2,function(err,targetBici){
                                    //console.log(targetBici)
                                    expect(targetBici.color).toBe(update.color);
                                    expect(targetBici.modelo).toBe(update.modelo);
                                    expect(targetBici.ubicacion[0]).toBe(update.ubicacion[0]);
                                    expect(targetBici.ubicacion[1]).toBe(update.ubicacion[1]);
                                    done();
                                });
                        });        
                    });
                });
            }); 
        });   
    });   
});






//se repite en cada ejecucion de un test
// beforeEach(()=>{Bicicleta.allBicis=[];});

// describe("Bicicletas.allBicis",()=>{
//     it("comienza vacia",()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var bici = new Bicicleta('1','rojo','urbana',[4.626408, -74.149127]);
//         Bicicleta.add(bici);
//         expect(Bicicleta.allBicis.length).toBe(1);
//     });
// });


// describe("Bicicleta.findById",()=>{
//     it("return bici with id 1",()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var abici1 = new Bicicleta('1','rojo','urbana',[4.626408, -74.149127]);
//         var abici2 = new Bicicleta('2','blanca','urbana',[4.628739, -74.145297]);
//         Bicicleta.add(abici1);
//         Bicicleta.add(abici2);
//         var targetBici=Bicicleta.findById(1);

//         expect(targetBici.id).toBe('1');
//         expect(targetBici.color).toBe(abici1.color);
//         expect(targetBici.modelo).toBe(abici1.modelo);
//         expect(targetBici.ubicacion[0]).toBe(abici1.ubicacion[0]);
//         expect(targetBici.ubicacion[1]).toBe(abici1.ubicacion[1]);
        
//     });
// });


// describe("Bicicleta.removeById",()=>{
//     it("return bici with id 1",()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var abici1 = new Bicicleta('1','rojo','urbana',[4.626408, -74.149127]);
//         var abici2 = new Bicicleta('2','blanca','urbana',[4.628739, -74.145297]);
//         Bicicleta.add(abici1);
//         Bicicleta.add(abici2);
//         Bicicleta.removeById(2);
//         var targetBici = Bicicleta.allBicis.find(x => x.id == 2);
//         expect(targetBici).toBe(undefined);
//     });
// });
